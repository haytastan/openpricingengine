from decimal import Decimal
from unittest.mock import patch

from pint import UnitRegistry
from django.test import TestCase
from django.utils import timezone

from .models import *


ureg = UnitRegistry()


class OrganizationTests(TestCase):
    def test_create_organization(self):
        cheap_used_clunkers = Organization(name='Cheap Used Clunkers')
        self.assertIsInstance(cheap_used_clunkers, Organization)

    def test_organization_name(self):
        cheap_used_clunkers = Organization(name='Cheap Used Clunkers')
        self.assertEqual(str(cheap_used_clunkers), 'Cheap Used Clunkers')

    def test_organization_name_not_null(self):
        # TODO: Prevent not null fields from being created; apparently not
        # possible without form validation?
        nameless_company = Organization()
        self.assertIsNotNone(nameless_company.name)

    def test_date_created(self):
        ice_cream_co = Organization()
        test_time = timezone.now()

        with patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = test_time
            ice_cream_co.save()

        self.assertEqual(ice_cream_co.date_created, test_time)


class ProductCollectionTests(TestCase):
    def test_creat_product_collection(self):
        clearance_items = ProductCollection(name='Clearance Items')
        self.assertIsInstance(clearance_items, ProductCollection)


class ProductTests(TestCase):
    def test_create_product(self):
        pizza_world = Organization()
        pizza_slice = Product(name="Pizza Slice", organization=pizza_world)
        self.assertIsInstance(pizza_slice, Product)

    def test_product_name(self):
        japanese_maple = Product(name='Japanese Maple')
        self.assertEqual(str(japanese_maple), 'Japanese Maple')


class ProductVariantTests(TestCase):
    def test_create_product_variant(self):
        t_shirt = Product(name='T-Shirt')
        t_shirt_size = ProductVariant(product=t_shirt)
        self.assertIsInstance(t_shirt_size, ProductVariant)

    def test_product_variant_name(self):
        t_shirt = Product(name='T-Shirt')
        t_shirt_size = ProductVariant(t_shirt, name='T-Shirt Size')
        self.assertEqual(str(t_shirt_size), 'T-Shirt Size')


class GoodTests(TestCase):
    def test_create_good(self):
        good = Good()
        self.assertIsInstance(good, Good)


class ServiceTests(TestCase):
    def test_create_service(self):
        service = Service()
        self.assertIsInstance(service, Service)


class PriceTests(TestCase):
    def test_create_price(self):
        pizza_slice = Product()
        price = Price(product=pizza_slice, amount=1.5)
        self.assertIsInstance(price, Price)

    def test_price_amount(self):
        pizza_slice = Product(name='Pizza Slice')
        price = Price(product=pizza_slice, amount=1.5)
        self.assertEqual(price.amount, 1.5)

    def test_price_str(self):
        pizza_slice = Product(name='Pizza Slice')
        price = Price(product=pizza_slice, amount=Decimal('1.50'))
        self.assertEqual(price.__str__(), 'USD 1.50')

    def test_price_quantity(self):
        six_ft_sub = Product(name='Six Foot Long Sub')
        foot = ureg.foot
        price = Price(product=six_ft_sub, quantity=6, unit=str(foot),
                      amount=Decimal('1.10'))
        self.assertEqual(price.__repr__(), Decimal('6.60'))


class OfferingTests(TestCase):
    def test_create_offering(self):
        offering = Offering()
        self.assertIsInstance(offering, Offering)
