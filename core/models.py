from django.db import models
from django.utils import timezone


# TODO: indicate competitors? Maybe in the analysis app? Yes.
class Organization(models.Model):
    """
    An Organization produces Offerings (Products at specific Prices).
    """
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    user_created = models.CharField(blank=True, max_length=50)
    user_updated = models.CharField(blank=True, max_length=50)

    def __str__(self):
        return self.name


class ProductCollection(models.Model):
    """
    A ProductCollection is a generic way to group products together, such as
    "Foods" and "Beverages." It's main purpose is to help logically organize
    products inside the system.
    """
    name = models.CharField(max_length=255)


class Product(models.Model):
    """
    A Product is a generic form of either a Good or a Service. This model will
    generalize fields and methods related to both products and services.
    """
    name = models.CharField(max_length=255)
    # TODO: Give warning; deletes all related!
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    collection = models.ForeignKey(ProductCollection, blank=True, null=True,
                                   on_delete=models.SET_NULL)
    # Should products be allowed in multiple collections? Maybe for retail
    # platforms, but not here?

    def __str__(self):
        return self.name


class ProductVariant(models.Model):
    """
    A ProductVariant is a Product with discrete and specific variations (size,
    color, speed) that a customer would normally choose between, which can often
    impact the Product's price. ProductVariants are linked to a Product for
    their core attributes and methods, but store the differentiating data.
    """
    name = models.CharField(max_length=255)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    # options = models.

    def __str__(self):
        return self.name


class Good(Product):
    """
    A Good is a tangible, often physical product to be consumed or otherwise
    used directly.
    """
    pass


class Service(Product):
    """
    A Service is an intangible, often non-material product to be provided.
    """
    pass


class Price(models.Model):
    """
    A Price is the specific number charged for a Product, determined by an often
    large number of variables.
    """
    product = models.ForeignKey(Product, blank=False, on_delete=models.PROTECT)
    quantity = models.IntegerField(default=1)
    # Stores a pint unit as a string
    unit = models.CharField(blank=True, max_length=100)
    # TODO: What should it round to?
    amount = models.DecimalField(default=0, max_digits=12, decimal_places=4)
    # Defined in ISO 4217 codes
    currency_code = models.CharField(default='USD', max_length=3)

    def calculate_price(self):
        return self.quantity * self.amount
    

    def __str__(self):
        return f'{self.currency_code} {self.calculate_price()}'

    def __repr__(self):
        return self.calculate_price()

# TODO: Discount, Credit, Fee, Tax, Penalty?


class Offering(models.Model):
    """
    An Offering is a collection of Goods and/or Services for a Price.
    """
    pass
